// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "AvacynGameMode.h"
#include "GameFramework/PlayerController.h"
#include "AvacynPlayerController.generated.h"

UCLASS()
class AAvacynPlayerController : public APlayerController
{
	GENERATED_BODY()


public:
	AAvacynPlayerController();

protected:
	/** True if the controlled character should navigate to the mouse cursor. */
	uint32 bMoveToMouseCursor : 1;

	// Begin PlayerController interface
	virtual void PlayerTick(float DeltaTime) override;
	virtual void SetupInputComponent() override;
	// End PlayerController interface

	/** Resets HMD orientation in VR. */
	void OnResetVR();

	/** Navigate player to the current mouse cursor location. */
	void MoveToMouseCursor();

	/** Navigate player to the current touch location. */
	void MoveToTouchLocation(const ETouchIndex::Type FingerIndex, const FVector Location);

	/** Navigate player to the given world location. */
	void SetNewMoveDestination(const FVector DestLocation);

	/** Input handlers for SetDestination action. */
	void OnSetDestinationPressed();
	void OnSetDestinationReleased();
	void OnSpellCastTestPressed();

	/** Variables used for handling key input for actionbar */
	UPROPERTY(BlueprintReadWrite, Category = "Player Controller")
	bool CastSpell;
	UPROPERTY(BlueprintReadOnly, Category = "Player Controller")
	uint8 keyPressed;
	UPROPERTY(BlueprintReadOnly, Category = "Player Controller")
	FVector TargetLocation;
private:
	// Variables and functions for saving and loading gameSave
	AAvacynGameMode* myGameMode;
	void SaveGame();
	void LoadGame();


	// Variables and functions for switching between the different Ingame menues
	void OpenMenuTalentTree();
	void OpenMenuInventory();
	void OpenMenuSkillWindow();
	void OpenMenuEquipmentAndStats();
	void OpenMenuQuestLog();
	void OpenDevelopmentTool();
	void TriggerInput1();
	void TriggerInput2();
	void TriggerInput3();
	void TriggerInput4();
	void TriggerInput5();
	void TriggerInput6();
	void TriggerInput7();
	void TriggerInput8();
	void TriggerInput9();
	void TriggerInput0();

};


