// Fill out your copyright notice in the Description page of Project Settings.

#include "Avacyn.h"
#include "Statistics.h"
#include "BaseProjectile.h"
#include "BaseEnemy.h"
#include "Fireball.h"

// Sets default values for this component's properties
UStatistics::UStatistics()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	Level = 100;
	CurrentExp = 0;
	ExpRewardOnKill = 0;
	bIsDualWielding = false;
	
	//CONSTRUCT THE GRAND MAP!
	{
		StatMap.Add(ETypeOfStat::RES_MaxHP, 100.0f);
		StatMap.Add(ETypeOfStat::RES_CurrentHP, 100.0f);
		StatMap.Add(ETypeOfStat::RES_HPPerSecond, 1.0f);
		StatMap.Add(ETypeOfStat::RES_MaxMana, 100.0f);
		StatMap.Add(ETypeOfStat::RES_CurrentMana, 100.0f);
		StatMap.Add(ETypeOfStat::RES_ManaPerSecond, 1.0f);

		StatMap.Add(ETypeOfStat::DEF_Armor, 0.0f);
		StatMap.Add(ETypeOfStat::DEF_Thorns, 0.0f);
		StatMap.Add(ETypeOfStat::DEF_MagicalBacklash, 0.0f);
		StatMap.Add(ETypeOfStat::DEF_PhysicalSave, 0.0f);
		StatMap.Add(ETypeOfStat::DEF_MentalSave, 0.0f);
		StatMap.Add(ETypeOfStat::DEF_DodgeChance, 0.0f);
		StatMap.Add(ETypeOfStat::DEF_BlockChance, 0.0f);
		StatMap.Add(ETypeOfStat::DEF_BlockAmount, 0.0f);

		StatMap.Add(ETypeOfStat::LO_LifeOnHit, 0.0f);
		StatMap.Add(ETypeOfStat::LO_LifeOnBlock, 0.0f);
		StatMap.Add(ETypeOfStat::LO_LifeOnDodge, 0.0f);
		StatMap.Add(ETypeOfStat::LO_LifeOnKill, 0.0f);
		StatMap.Add(ETypeOfStat::LO_LifePerSecond, 0.0f);
		StatMap.Add(ETypeOfStat::LO_LifePerResourceSpent, 0.0f);

		StatMap.Add(ETypeOfStat::UTIL_MovementSpeed, 1.0f);
		StatMap.Add(ETypeOfStat::UTIL_CooldownReduction, 0.0f);
		StatMap.Add(ETypeOfStat::UTIL_AoeSize, 1.0f);
		StatMap.Add(ETypeOfStat::UTIL_ProjectileRange, 1.0f);
		StatMap.Add(ETypeOfStat::UTIL_SpellRange, 1.0f);
		StatMap.Add(ETypeOfStat::UTIL_CleaveRadius, 1.0f);
		StatMap.Add(ETypeOfStat::UTIL_ChanceToFreezeOnHit, 0.0f);
		StatMap.Add(ETypeOfStat::UTIL_ChanceToStunOnHit, 0.0f);
		StatMap.Add(ETypeOfStat::UTIL_ChanceToSlowOnHit, 0.0f);
		StatMap.Add(ETypeOfStat::UTIL_CurseDuration, 1.0f);
		StatMap.Add(ETypeOfStat::UTIL_CurseEffect, 1.0f);
		StatMap.Add(ETypeOfStat::UTIL_MaxNumberOfCurses, 1.0f);

		StatMap.Add(ETypeOfStat::POT_PotionStrength, 1.0f);
		StatMap.Add(ETypeOfStat::POT_PotionDuration, 1.0f);

		StatMap.Add(ETypeOfStat::MINI_MinionMovementSpeed, 1.0f);
		StatMap.Add(ETypeOfStat::MINI_MinionAoeSize, 1.0f);
		StatMap.Add(ETypeOfStat::MINI_MinionAoeDamage, 1.0f);
		StatMap.Add(ETypeOfStat::MINI_MinionDamage, 1.0f);

		StatMap.Add(ETypeOfStat::OFF_EliteDamage, 1.0f);
		StatMap.Add(ETypeOfStat::OFF_AoeDamage, 1.0f);
		StatMap.Add(ETypeOfStat::OFF_ProjectileSpeed, 1.0f);
		StatMap.Add(ETypeOfStat::OFF_AdditionalChainAndBounceTargets, 0.0f);
		StatMap.Add(ETypeOfStat::OFF_ChanceToIgniteOnHit, 0.0f);
		StatMap.Add(ETypeOfStat::OFF_ChanceToBleedOnHit, 0.0f);
		StatMap.Add(ETypeOfStat::OFF_ChanceToPoisonOnHit, 0.0f);
		StatMap.Add(ETypeOfStat::OFF_BurnBaseDamage, 0.0f);//NEEDED?
		StatMap.Add(ETypeOfStat::OFF_BleedBaseDamage, 0.0f);//NEEDED?
		StatMap.Add(ETypeOfStat::OFF_PoisonBaseDamage, 0.0f);//NEEDED?

		StatMap.Add(ETypeOfStat::OFFM_BaseMagicPower, 1.0f);
		StatMap.Add(ETypeOfStat::OFFM_MagicPower, 1.0f);
		StatMap.Add(ETypeOfStat::OFFM_SpellCritChance, 0.0f);
		StatMap.Add(ETypeOfStat::OFFM_SpellCritDamage, 2.0f);
		StatMap.Add(ETypeOfStat::OFFM_CastingSpeed, 1.0f);

		StatMap.Add(ETypeOfStat::OFFP_PhysicalCritChance, 0.0f);
		StatMap.Add(ETypeOfStat::OFFP_PhysicalCritDamage, 2.0f);
		StatMap.Add(ETypeOfStat::OFFP_PhysicalBaseDamageMin, 1.0f);
		StatMap.Add(ETypeOfStat::OFFP_PhysicalBaseDamageMax, 1.0f);
		StatMap.Add(ETypeOfStat::OFFP_AttackSpeedModifier, 1.0f);
		StatMap.Add(ETypeOfStat::OFFP_CooldownBetweenAttacks, 1.0f);
		StatMap.Add(ETypeOfStat::OFFP_DualWieldingAttackSpeed, 1.0f);

		StatMap.Add(ETypeOfStat::DI_PhysicalDamage, 0.0f);
		StatMap.Add(ETypeOfStat::DI_FireDamage, 0.0f);
		StatMap.Add(ETypeOfStat::DI_FrostDamage, 0.0f);
		StatMap.Add(ETypeOfStat::DI_LightningDamage, 0.0f);
		StatMap.Add(ETypeOfStat::DI_HolyDamage, 0.0f);
		StatMap.Add(ETypeOfStat::DI_ShadowDamage, 0.0f);
		StatMap.Add(ETypeOfStat::DI_NatureDamage, 0.0f);
		StatMap.Add(ETypeOfStat::DI_PoisonDamage, 0.0f);
		StatMap.Add(ETypeOfStat::DI_AcidDamage, 0.0f);
		StatMap.Add(ETypeOfStat::DI_BleedDamage, 0.0f);
		StatMap.Add(ETypeOfStat::DI_ChaosDamage, 0.0f);

		StatMap.Add(ETypeOfStat::DIW_DamageIncreaseWithSword_1H, 0.0f);
		StatMap.Add(ETypeOfStat::DIW_DamageIncreaseWithSword_2H, 0.0f);
		StatMap.Add(ETypeOfStat::DIW_DamageIncreaseWithMace_1H, 0.0f);
		StatMap.Add(ETypeOfStat::DIW_DamageIncreaseWithMace_2H, 0.0f);
		StatMap.Add(ETypeOfStat::DIW_DamageIncreaseWithAxe_1H, 0.0f);
		StatMap.Add(ETypeOfStat::DIW_DamageIncreaseWithAxe_2H, 0.0f);
		StatMap.Add(ETypeOfStat::DIW_DamageIncreaseWithShield_MH, 0.0f);
		StatMap.Add(ETypeOfStat::DIW_DamageIncreaseWithShield_OH, 0.0f);
		StatMap.Add(ETypeOfStat::DIW_DamageIncreaseWithDagger, 0.0f);
		StatMap.Add(ETypeOfStat::DIW_DamageIncreaseWithFist, 0.0f);
		StatMap.Add(ETypeOfStat::DIW_DamageIncreaseWithStaff, 0.0f);
		StatMap.Add(ETypeOfStat::DIW_DamageIncreaseWithQuiver, 0.0f);
		StatMap.Add(ETypeOfStat::DIW_DamageIncreaseWithSpellbook, 0.0f);
		StatMap.Add(ETypeOfStat::DIW_DamageIncreaseWithScythe, 0.0f);
		StatMap.Add(ETypeOfStat::DIW_DamageIncreaseWithElementalOrb, 0.0f);
		StatMap.Add(ETypeOfStat::DIW_DamageIncreaseWithBow, 0.0f);

		StatMap.Add(ETypeOfStat::DR_DamageResistancePhysical, 0.0f);
		StatMap.Add(ETypeOfStat::DR_DamageResistanceFire, 0.0f);
		StatMap.Add(ETypeOfStat::DR_DamageResistanceFrost, 0.0f);
		StatMap.Add(ETypeOfStat::DR_DamageResistanceLightning, 0.0f);
		StatMap.Add(ETypeOfStat::DR_DamageResistanceHoly, 0.0f);
		StatMap.Add(ETypeOfStat::DR_DamageResistanceShadow, 0.0f);
		StatMap.Add(ETypeOfStat::DR_DamageResistanceNature, 0.0f);
		StatMap.Add(ETypeOfStat::DR_DamageResistancePoison, 0.0f);
		StatMap.Add(ETypeOfStat::DR_DamageResistanceAcid, 0.0f);
		StatMap.Add(ETypeOfStat::DR_DamageResistanceBleed, 0.0f);
		StatMap.Add(ETypeOfStat::DR_DamageResistanceChaos, 0.0f);

		StatMap.Add(ETypeOfStat::DR_DamageReduction, 0.0f);
	}
	//END OF THE GRAND MAP CONSTRUCTION. HOOORAAAAY!!!

	// Construct the HandleDamageMap
	{
		void(*ReceiveDmg)(ETypeOfDamage DamageType, FProjectileDamageData ProjectileDamageData, UStatistics* self);

		ReceiveDmg = UStatistics::ReceivePhysicalDamage;
		HandleDamageMap.Emplace(ETypeOfDamage::Physical, ReceiveDmg);

		ReceiveDmg = UStatistics::ReceiveChaosDamage;
		HandleDamageMap.Emplace(ETypeOfDamage::Chaos, ReceiveDmg);
		
		ReceiveDmg = UStatistics::ReceiveElementalDamage;
		HandleDamageMap.Emplace(ETypeOfDamage::Fire, ReceiveDmg);
		HandleDamageMap.Emplace(ETypeOfDamage::Frost, ReceiveDmg);
		HandleDamageMap.Emplace(ETypeOfDamage::Lightning, ReceiveDmg);
		HandleDamageMap.Emplace(ETypeOfDamage::Holy, ReceiveDmg);
		HandleDamageMap.Emplace(ETypeOfDamage::Shadow, ReceiveDmg);
		HandleDamageMap.Emplace(ETypeOfDamage::Nature, ReceiveDmg);
		HandleDamageMap.Emplace(ETypeOfDamage::Poison, ReceiveDmg);
		HandleDamageMap.Emplace(ETypeOfDamage::Acid, ReceiveDmg);
		HandleDamageMap.Emplace(ETypeOfDamage::Bleed, ReceiveDmg);
	}

	// Construct the DamageToResistanceMap
	{
		DamageToResistanceMap.Add(ETypeOfDamage::Fire, ETypeOfStat::DR_DamageResistanceFire);
		DamageToResistanceMap.Add(ETypeOfDamage::Frost, ETypeOfStat::DR_DamageResistanceFrost);
		DamageToResistanceMap.Add(ETypeOfDamage::Lightning, ETypeOfStat::DR_DamageResistanceLightning);
		DamageToResistanceMap.Add(ETypeOfDamage::Holy, ETypeOfStat::DR_DamageResistanceHoly);
		DamageToResistanceMap.Add(ETypeOfDamage::Shadow, ETypeOfStat::DR_DamageResistanceShadow);
		DamageToResistanceMap.Add(ETypeOfDamage::Nature, ETypeOfStat::DR_DamageResistanceNature);
		DamageToResistanceMap.Add(ETypeOfDamage::Poison, ETypeOfStat::DR_DamageResistancePoison);
		DamageToResistanceMap.Add(ETypeOfDamage::Acid, ETypeOfStat::DR_DamageResistanceAcid);
		DamageToResistanceMap.Add(ETypeOfDamage::Bleed, ETypeOfStat::DR_DamageResistanceBleed);
		DamageToResistanceMap.Add(ETypeOfDamage::Chaos, ETypeOfStat::DR_DamageResistanceChaos);
		DamageToResistanceMap.Add(ETypeOfDamage::Physical, ETypeOfStat::DR_DamageResistancePhysical);
	}

	// Construct the ResourceToStatMap
	{
		ResourceToStatMap.Add(ETypeOfResource::Health, ETypeOfStat::RES_CurrentHP);
		ResourceToStatMap.Add(ETypeOfResource::Mana, ETypeOfStat::RES_CurrentMana);
	}

	// Construct the LevelToArmorMultiplierMap
	{
		float ArmorMultiplier = 0.01f;

		// 0.8f requires you to have about 100 billion armor to get a two digit damage reduction percentage at level 100
		// 0.9f requires you to have about   1 million armor to get a two digit damage reduction percentage at level 100
		float ArmorScaleFactorPerLevel = 0.9f;
		for (int i = 1; i < 100; i++)
		{
			LevelToArmorMultiplierMap.Add(i, ArmorMultiplier);
			ArmorMultiplier *= ArmorScaleFactorPerLevel;
		}
	}
}


// Called when the game starts
void UStatistics::BeginPlay()
{
	Super::BeginPlay();

	// ...

}


// Called every frame
void UStatistics::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	float tempCurrentHP = StatMap.FindRef(ETypeOfStat::RES_CurrentHP);
	float tempCurrentMana = StatMap.FindRef(ETypeOfStat::RES_CurrentMana);
	float tempHPRegen = StatMap.FindRef(ETypeOfStat::RES_HPPerSecond);
	float tempManaRegen = StatMap.FindRef(ETypeOfStat::RES_ManaPerSecond);
	//tempCurrentHP -= 1.0f * DeltaTime;
	//StatMap.Add(ETypeOfStat::RES_CurrentHP, tempCurrentHP);

	tempCurrentMana += tempManaRegen * DeltaTime;
	tempCurrentHP += tempHPRegen * DeltaTime;
	if (tempCurrentHP > StatMap.FindRef(ETypeOfStat::RES_MaxHP))
	{
		tempCurrentHP = StatMap.FindRef(ETypeOfStat::RES_MaxHP);
	}
	if (tempCurrentMana > StatMap.FindRef(ETypeOfStat::RES_MaxMana))
	{
		tempCurrentMana = StatMap.FindRef(ETypeOfStat::RES_MaxMana);
	}
	StatMap.Add(ETypeOfStat::RES_CurrentHP, tempCurrentHP);
	StatMap.Add(ETypeOfStat::RES_CurrentMana, tempCurrentMana);

}

float UStatistics::GetStat(ETypeOfStat stat)
{
	return StatMap.FindRef(stat);
}

void UStatistics::ModifyStat(ETypeOfStat stat, float value)
{
	//get the current value
	float tempStat = StatMap.FindRef(stat);

	//adds the value
	tempStat += value;

	//put the value back
	StatMap.Add(stat, tempStat);
}


void UStatistics::ReceiveDamage(FProjectileDamageData ProjectileDamageData)
{
	// if player character, try to dodge

	for (auto i : ProjectileDamageData.Damage)
	{	
		HandleDamageMap.FindRef(i.Key)(i.Key, ProjectileDamageData, this);
	}
}

void UStatistics::ReceivePhysicalDamage(ETypeOfDamage DamageType, FProjectileDamageData ProjectileDamageData, UStatistics* self)
{ 
	float ArmorEffectivenessMultiplier = self->LevelToArmorMultiplierMap.FindRef(ProjectileDamageData.InstigatorLevel);

	float Resistance = self->GetStat(ETypeOfStat::DEF_Armor) * ArmorEffectivenessMultiplier;
	float PenetrationDamageSpellHas = ProjectileDamageData.DamagePenetrationPercentage.FindRef(DamageType);
	float DamageReduction = self->StatMap.FindRef(ETypeOfStat::DR_DamageReduction);

	float BaseDamageSpellDeals = ProjectileDamageData.Damage.FindRef(DamageType);
	if (self->IsElite())
	{
		BaseDamageSpellDeals *= ProjectileDamageData.EliteDamageModifier;
	}
	float ResistanceAndPenetration = 1 - (Resistance - PenetrationDamageSpellHas);
	float LevelDifferenceModifier = 1 - ((self->Level - ProjectileDamageData.InstigatorLevel) / 20);
	if (LevelDifferenceModifier < 0.0f)
	{
		LevelDifferenceModifier = 0.0f;
	}
	float DamageReductionModifier = 1.0f - DamageReduction;

	float Result = BaseDamageSpellDeals * ResistanceAndPenetration * LevelDifferenceModifier * DamageReductionModifier;

	self->ApplyDamage(Result);
}

void UStatistics::ReceiveElementalDamage(ETypeOfDamage DamageType, FProjectileDamageData ProjectileDamageData, UStatistics* self)
{
	// TODO: implement function to receive magical backlash (and thorns)
	//magical backlash: ProjectileDamageData.Instigator->ApplyMagicalBacklash(uint32 damage); <- something like that
	
	float Resistance = self->StatMap.FindRef(self->DamageToResistanceMap.FindRef(DamageType));
	float PenetrationDamageSpellHas = ProjectileDamageData.DamagePenetrationPercentage.FindRef(DamageType);
	float DamageReduction = self->StatMap.FindRef(ETypeOfStat::DR_DamageReduction);

	float BaseDamageSpellDeals = ProjectileDamageData.Damage.FindRef(DamageType);
	if (self->IsElite())
	{
		BaseDamageSpellDeals *= ProjectileDamageData.EliteDamageModifier;
	}
	float ResistanceAndPenetration = 1 - (Resistance - PenetrationDamageSpellHas);
	float LevelDifferenceModifier = 1 - ((self->Level - ProjectileDamageData.InstigatorLevel) / 20);
	if (LevelDifferenceModifier < 0.0f)
	{
		LevelDifferenceModifier = 0.0f;
	}
	float DamageReductionModifier = 1.0f - DamageReduction;

	float Result = BaseDamageSpellDeals * ResistanceAndPenetration * LevelDifferenceModifier * DamageReductionModifier;

	self->ApplyDamage(Result);
}

void UStatistics::ReceiveChaosDamage(ETypeOfDamage DamageType, FProjectileDamageData ProjectileDamageData, UStatistics* self)
{
	float Resistance = self->StatMap.FindRef(self->DamageToResistanceMap.FindRef(DamageType));
	float PenetrationDamageSpellHas = ProjectileDamageData.DamagePenetrationPercentage.FindRef(DamageType);

	float BaseDamageSpellDeals = ProjectileDamageData.Damage.FindRef(DamageType);
	if (self->IsElite())
	{
		BaseDamageSpellDeals *= ProjectileDamageData.EliteDamageModifier;
	}
	float ResistanceAndPenetration = 1 - (Resistance - PenetrationDamageSpellHas);
	float LevelDifferenceModifier = 1 - ((self->Level - ProjectileDamageData.InstigatorLevel) / 20);
	if (LevelDifferenceModifier < 0.0f)
	{
		LevelDifferenceModifier = 0.0f;
	}

	float Result = BaseDamageSpellDeals * ResistanceAndPenetration * LevelDifferenceModifier;

	self->ApplyDamage(Result);
}

void UStatistics::ApplyDamage(float Damage)
{
	float CurrentHP = StatMap.FindRef(ETypeOfStat::RES_CurrentHP);
	float MaxHP = StatMap.FindRef(ETypeOfStat::RES_MaxHP);

	CurrentHP -= Damage;

	if (CurrentHP > MaxHP)
	{
		CurrentHP = MaxHP;
	}
	else if (CurrentHP < 0)
	{
		ABaseEnemy* ThisCharacter = Cast<ABaseEnemy>(this->GetOwner());
		if (ThisCharacter)
		{
			ThisCharacter->Die();
		}
	}

	StatMap.Add(ETypeOfStat::RES_CurrentHP, CurrentHP);
}

bool UStatistics::IsElite()
{
	return bIsElite;
}

void UStatistics::SetIsElite(bool NewIsElite)
{
	bIsElite = NewIsElite;
}

uint8 UStatistics::GetLevel()
{
	return static_cast<uint8>(Level);
}

uint8 UStatistics::GetExperience()
{
	return static_cast<uint8>(CurrentExp);
}
float UStatistics::GetResource(ETypeOfResource ResourceType)
{
	return GetStat(ResourceToStatMap.FindRef(ResourceType));
}

void UStatistics::SpendResource(ETypeOfResource ResourceType, float AmountToSpend)
{
	ModifyStat(ResourceToStatMap.FindRef(ResourceType), -AmountToSpend);
}
