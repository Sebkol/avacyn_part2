// Fill out your copyright notice in the Description page of Project Settings.

#include "Avacyn.h"
#include "TalentTree.h"


// Sets default values for this component's properties
UTalentTree::UTalentTree()
{
	PrimaryComponentTick.bCanEverTick = true;
}


// Called when the game starts
void UTalentTree::BeginPlay()
{
	Super::BeginPlay();

	// ...

}


// Called every frame
void UTalentTree::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UTalentTree::ApplyTalentInfoFromSave(TMap<ETypeOfTalentTree, FTalentTreeAllocationInfo> SaveTalentInfo)
{

	AllocatedTalents.Empty();
	AllocatedTalents.Append(SaveTalentInfo);
}

void UTalentTree::AllocatePointFromTalentTree(ETypeOfTalentTree TalentTree, ETalentName TalentName, uint8 AllocatedPoints)
{

	auto TalentTreeInfo = AllocatedTalents.Find(TalentTree);
	if (TalentTreeInfo)
	{
		TalentTreeInfo->PointsAllocated.Add(TalentName, AllocatedPoints);
	}
	else
	{
		//TalentTreeInfo = new FTalentTreeAllocationInfo;
		FTalentTreeAllocationInfo newTalentInfo;
		newTalentInfo.AssociatedTalentTree = TalentTree;
		newTalentInfo.PointsAllocated.Add(TalentName, AllocatedPoints);
		AllocatedTalents.Add(TalentTree, newTalentInfo);
	}
}

uint8 UTalentTree::GetNumPointsAllocated()
{
	uint8 NumPointsAllocated = 0;
	for (auto TalentTree: AllocatedTalents)
	{
		for (auto Talent: TalentTree.Value.PointsAllocated)
		{
			NumPointsAllocated += Talent.Value;
		}
	}
	return NumPointsAllocated;
}