// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Structs.h"
#include "BaseProjectile.generated.h"

UCLASS()
class AVACYN_API ABaseProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABaseProjectile();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called when something enters the sphere collision component
	UFUNCTION()
	virtual void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void SetProjectileData(FProjectileBehaviorData ProjectileBehaviorData, FProjectileDamageData ProjectileDamageData);

	UFUNCTION()
	virtual void Initialize();

	virtual void HitActor(AActor* OtherActor);
	
protected:
	FProjectileBehaviorData ProjectileBehaviorData;
	FProjectileDamageData ProjectileDamageData;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Projectile", meta = (AllowPrivateAccess = "true"))
	class USphereComponent* ProjectileCollisionSphere;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Projectile", meta = (AllowPrivateAccess = "true"))
	UProjectileMovementComponent* ProjectileMovementComponent;

	FVector SpawnLocation;
};
