// Fill out your copyright notice in the Description page of Project Settings.

#include "Avacyn.h"
#include "AvacynSaveGame.h"
#include "AvacynGameInstance.h"



UAvacynSaveGame::UAvacynSaveGame()
{
	//default value
	PlayerLocation = FVector(50.0f, 50.0f, 10000.0f);
	CharacterName = TEXT("Player1");
	CharacterSkeletalMesh = CreateDefaultSubobject<USkeletalMesh>(TEXT("SkeletalMesh'/Game/CharacterModels/InfinityBladeCharacters/SK_CharM_Barbarous.SK_CharM_Barbarous'"));
	SaveInventoryItems = {};
	SaveInventoryItems.Reserve(10000);

	
}


void UAvacynSaveGame::SetPlayerLocation(FVector newLocation) {
	PlayerLocation = newLocation;
}

FVector UAvacynSaveGame::GetPlayerLocation() {
	return PlayerLocation;
}
FString UAvacynSaveGame::getCharacterName()
{
	return CharacterName;
}

USkeletalMesh* UAvacynSaveGame::GetCharacterSkeletalMesh()
{
	return CharacterSkeletalMesh;
}
void UAvacynSaveGame::SetCharacterName(FString newName) {

	CharacterName = newName;

	auto GameInstance = Cast<UAvacynGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	if (GameInstance->IsDebugMode())
	{
		GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Red, TEXT("new Character name: " + CharacterName));

	}
}