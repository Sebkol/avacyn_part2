// Fill out your copyright notice in the Description page of Project Settings.

#include "Avacyn.h"
#include "BaseEnemy.h"
#include "AvacynCharacter.h"
#include "BaseProjectile.h"


// Sets default values
ABaseProjectile::ABaseProjectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Add collision sphere
	ProjectileCollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Sphere Collision Component"));
	RootComponent = ProjectileCollisionSphere;
	ProjectileCollisionSphere->InitSphereRadius(30.0f);
	ProjectileCollisionSphere->SetCollisionProfileName(TEXT("Projectile"));
	ProjectileCollisionSphere->OnComponentBeginOverlap.AddDynamic(this, &ABaseProjectile::OnOverlapBegin);

	// Add Projectile Movement Component
	ProjectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Projectile Movement Component"));
	ProjectileMovementComponent->ProjectileGravityScale = 0.0f;
	ProjectileMovementComponent->InitialSpeed = 0.0f;
	this->AddOwnedComponent(ProjectileMovementComponent);

}

// Called when the game starts or when spawned
void ABaseProjectile::BeginPlay()
{
	Super::BeginPlay();
	
	SpawnLocation = this->GetActorLocation();
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, FString::Printf(TEXT("Projectile SpawnLocation X: %f, Y: %f, Z: %f"), SpawnLocation.X, SpawnLocation.Y, SpawnLocation.Z));
}

// Called every frame
void ABaseProjectile::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

	// Destroys the projectile if it has traveled past its MaxTravelDistance
	FVector CurrentLocation = this->GetActorLocation();
	if (FVector::Dist(SpawnLocation, CurrentLocation) > ProjectileBehaviorData.MaxTravelDistance)
	{
		
		this->Destroy();
	}
}

void ABaseProjectile::OnOverlapBegin(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	// TODO: change instigator == overlapped unit checks to check against tags(?) instead
	// Reason: Right now different enemies and players will be able to hit eachother

	// Check if an enemy character was hit
	ABaseEnemy* CharacterOverlapped = Cast<ABaseEnemy>(OtherActor);
	if (CharacterOverlapped)
	{
		// check if it was the enemy character that shot the projectile that hit him
		if (ProjectileDamageData.Instigator != GetWorld()->GetFirstPlayerController()->GetPawn())
		{
			//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("Hit Enemy, but enemy can't hit himself!"));
			return;
		}
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, TEXT("Hit Enemy!"));
		HitActor(OtherActor);
		return;
	}

	// check if the player character was hit
	AAvacynCharacter* PlayerOverlapped = Cast<AAvacynCharacter>(OtherActor);
	if (PlayerOverlapped)
	{
		// check if it was the player character that shot the projectile that hit him
		if (ProjectileDamageData.Instigator == PlayerOverlapped)
		{
			//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("Hit Player, but player can't hit himself!"));
			return;
		}
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, TEXT("Hit Player!"));
		HitActor(OtherActor);
		return;
	}
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Hit Unknown!"));
}

void ABaseProjectile::SetProjectileData(FProjectileBehaviorData ProjectileBehaviorData, FProjectileDamageData ProjectileDamageData)
{
	this->ProjectileBehaviorData = ProjectileBehaviorData;
	this->ProjectileDamageData = ProjectileDamageData;
}

void ABaseProjectile::Initialize()
{
	ProjectileMovementComponent->MaxSpeed = ProjectileBehaviorData.ProjectileSpeed;
	ProjectileMovementComponent->InitialSpeed = ProjectileBehaviorData.ProjectileSpeed;
}

void ABaseProjectile::HitActor(AActor* OtherActor)
{
	ABaseEnemy* EnemyHit = Cast<ABaseEnemy>(OtherActor);
	AAvacynCharacter* PlayerHit = Cast<AAvacynCharacter>(OtherActor);

	if (EnemyHit)
	{
		EnemyHit->ReceiveDamage(ProjectileDamageData);
	}
	else if (PlayerHit)
	{
		PlayerHit->ReceiveDamage(ProjectileDamageData);
	}
	else
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Something was hit, but casts failed!"));
	}

	this->Destroy();
}