// Fill out your copyright notice in the Description page of Project Settings.

#include "Avacyn.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyAllTypes.h"
#include "AvacynCharacter.h"
#include "EnemyAI.h"
#include "BaseEnemy.h"
#include "BTTask_MoveToPlayer.h"




EBTNodeResult::Type UBTTask_MoveToPlayer::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	AEnemyAI* CharacterPlayerController = Cast<AEnemyAI>(OwnerComp.GetAIOwner());

	AAvacynCharacter* Enemy = Cast<AAvacynCharacter>(OwnerComp.GetBlackboardComponent()->GetValue<UBlackboardKeyType_Object>(CharacterPlayerController->EnemyKeyID));
	
	float DesiredRangeToTarget = 500.0f;

	if (Enemy)
	{
		if (CharacterPlayerController->GetPawn()->GetDistanceTo(Enemy) < DesiredRangeToTarget)
		{
			return EBTNodeResult::Succeeded;
			//return EBTNodeResult::Failed;
		}

		if (CharacterPlayerController->LineOfSightTo(Enemy))
		{
			CharacterPlayerController->MoveToActor(Enemy, DesiredRangeToTarget, true, true, false, 0, true);
		}
		else
		{
			CharacterPlayerController->MoveToActor(Enemy, 5.0f, true, true, false, 0, true);
		}
		return EBTNodeResult::Failed;
		//return EBTNodeResult::Failed;
	}
	else
	{
		return EBTNodeResult::Failed;
		//return EBTNodeResult::Succeeded;
	}
}