// Fill out your copyright notice in the Description page of Project Settings.

#include "Avacyn.h"
#include "AvacynCharacter.h"
#include "EquippedItems.h"


// Sets default values for this component's properties
UEquippedItems::UEquippedItems()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...

	//initializing the map
	//FGuid tempGUID;
	//MyEquippedItems.Add(ETypeOfEquipmentSlot::Amulet, tempGUID);
	//MyEquippedItems.Add(ETypeOfEquipmentSlot::Ancient_Relic, tempGUID);
	//MyEquippedItems.Add(ETypeOfEquipmentSlot::Back, tempGUID);
	//MyEquippedItems.Add(ETypeOfEquipmentSlot::Boots, tempGUID);
	//MyEquippedItems.Add(ETypeOfEquipmentSlot::Chest, tempGUID);
	//MyEquippedItems.Add(ETypeOfEquipmentSlot::Flask_1, tempGUID);
	//MyEquippedItems.Add(ETypeOfEquipmentSlot::Flask_2, tempGUID);
	//MyEquippedItems.Add(ETypeOfEquipmentSlot::Flask_3, tempGUID);
	//MyEquippedItems.Add(ETypeOfEquipmentSlot::Gloves, tempGUID);
	//MyEquippedItems.Add(ETypeOfEquipmentSlot::Helmet, tempGUID);
	//MyEquippedItems.Add(ETypeOfEquipmentSlot::Legs, tempGUID);
	//MyEquippedItems.Add(ETypeOfEquipmentSlot::Mainhand, tempGUID);
	//MyEquippedItems.Add(ETypeOfEquipmentSlot::Offhand, tempGUID);
	//MyEquippedItems.Add(ETypeOfEquipmentSlot::Ring_1, tempGUID);
	//MyEquippedItems.Add(ETypeOfEquipmentSlot::Ring_2, tempGUID);
	//MyEquippedItems.Add(ETypeOfEquipmentSlot::Shoulder, tempGUID);

}


// Called when the game starts
void UEquippedItems::BeginPlay()
{
	Super::BeginPlay();

	// ...

}


// Called every frame
void UEquippedItems::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

/*
Called from blueprints, requesting a slot to be equipped.
*/
bool UEquippedItems::EquipRequest(FGuid ItemId)
{
	//TODO: check level requirement..
	auto Pawn = Cast<AAvacynCharacter>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
	if (Pawn)
	{
		//gets a reference to the players statistics component as the stats on the item should modify the statistics component.
		auto statistics = Pawn->GetStatisticsComponent();
		//gets a reference to the inventory in order to get the stats of the item being equipped.
		auto inventory = Pawn->GetInventoryComponent();
		//find which item the player wish to equip
		auto itemToEquip = inventory->GetInventoryItem(ItemId);
		if (FindAssociatedEquipmentSlot(itemToEquip.Type) == ETypeOfEquipmentSlot::NONE)
		{
			//not an equippable item
			return false;
		}
		if (itemToEquip.LevelRequirement > statistics->GetLevel())
		{
			//returns false if the item requirement is higher than the players level
			return false;
		}
		return Equip(ItemId, FindAssociatedEquipmentSlot(itemToEquip.Type));
	}
	return false;
}
/*
	Called from blueprints, requesting a slot to be unequipped.
*/
bool UEquippedItems::UnequipRequest(ETypeOfEquipmentSlot slot)
{
	if (MyEquippedItems.Find(slot) != nullptr)
	{
		Unequip(slot);
		return true;
	}
	else
	{
		return false;
	}
}

/*
	Tries to unequip the item located in the equipmentslot provided as parameter.
*/
void UEquippedItems::Unequip(ETypeOfEquipmentSlot slot)
{
	auto result = MyEquippedItems.FindRef(slot);
	auto Pawn = Cast<AAvacynCharacter>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
	if (Pawn)
	{
		auto statistics = Pawn->GetStatisticsComponent();
		auto inventory = Pawn->GetInventoryComponent();

		auto itemEquipped = inventory->GetInventoryItem(result);
		for (auto stat : itemEquipped.StatsOnItem)
		{
			statistics->ModifyStat(stat.Key, stat.Value * -1.0f);
		}
	}
	//removes the equipped item from record. This is because we intend to save the equippedItems component to file, 
	//so when we load up the character, we dont want old items equipped to still be lying around.
	MyEquippedItems.Remove(slot);
}

//Tries to equip the item sent as parameter. returns true if it successfully equipped, false if nothing was equipped.
bool UEquippedItems::Equip(FGuid newItemId, ETypeOfEquipmentSlot slot)
{
	//modified in case one of the checks done later turns out to change the desired slot. 
	ETypeOfEquipmentSlot SlotTouse = slot;

	//TODO: add logic for 1h/2h/offhand 
	auto Pawn = Cast<AAvacynCharacter>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
	if (Pawn)
	{
		//gets a reference to the players statistics component as the stats on the item should modify the statistics component.
		auto statistics = Pawn->GetStatisticsComponent();
		//gets a reference to the inventory in order to get the stats of the item being equipped.
		auto inventory = Pawn->GetInventoryComponent();
		//find which item the player wish to equip
		auto itemToEquip = inventory->GetInventoryItem(newItemId);


		//Special logic only required for weapons
		if (SlotTouse == ETypeOfEquipmentSlot::Mainhand || SlotTouse == ETypeOfEquipmentSlot::Offhand)
		{
			FItem mainhandItem;
			FItem offhandItem;

			//get item currently in main hand
			auto itemInMainHand = MyEquippedItems.Find(ETypeOfEquipmentSlot::Mainhand);
			if (itemInMainHand != nullptr)
			{
				mainhandItem = inventory->GetInventoryItem(*itemInMainHand);
			}
			//gets item currently in offhand
			auto itemInOffhand = MyEquippedItems.Find(ETypeOfEquipmentSlot::Offhand);
			if (itemInOffhand != nullptr)
			{
				offhandItem = inventory->GetInventoryItem(*itemInOffhand);
			}
			//Equip logic for 1h in main hand:
			if (IsOneHanded(itemToEquip.Type))
			{
				if (SlotTouse == ETypeOfEquipmentSlot::Mainhand)
				{
					if (offhandItem.Type == ETypeOfItem::Quiver)
					{
						//Unequip quiver
						Unequip(ETypeOfEquipmentSlot::Offhand);
					}
				}
				else
				{
					if (IsTwoHanded(mainhandItem.Type))
					{
						//unequip main hand
						Unequip(ETypeOfEquipmentSlot::Mainhand);
						SlotTouse = ETypeOfEquipmentSlot::Mainhand;
					}
				}
			}
			//Equip logic for 2h
			else if (IsTwoHanded(itemToEquip.Type))
			{
				if (offhandItem.Type == ETypeOfItem::Quiver && itemToEquip.Type == ETypeOfItem::Bow)
				{
					//Do nothing
				}
				else
				{
					//unequip offhand
					Unequip(ETypeOfEquipmentSlot::Offhand);
				}
			}
			//equip logic for offhand
			else
			{
				if (IsTwoHanded(mainhandItem.Type))
				{
					if (itemToEquip.Type == ETypeOfItem::Quiver && mainhandItem.Type == ETypeOfItem::Bow)
					{
						// do nothing
					}
					else
					{
						// unequip main hand
						Unequip(ETypeOfEquipmentSlot::Mainhand);
					}
				}
				else
				{
					if (itemToEquip.Type == ETypeOfItem::Quiver)
					{
						Unequip(ETypeOfEquipmentSlot::Mainhand);
					}
				}
			}
		}

		//unequip desired slot
		if (MyEquippedItems.Find(SlotTouse) != nullptr)
		{
			Unequip(SlotTouse);
		}

		//equip ItemToEquip
		auto result = MyEquippedItems.FindRef(SlotTouse);
		result = newItemId;
		//for each stat on newly equipped item, modify the statistics component. 
		for (auto stat : itemToEquip.StatsOnItem)
		{
			statistics->ModifyStat(stat.Key, stat.Value);
		}
		MyEquippedItems.Add(SlotTouse, result);
	}
	return true;
}
/*
	Checks if the offhand has an item equipped in it. returns true if it has, false if not.
*/
bool UEquippedItems::IsOffhandAvailable()
{
	auto result = MyEquippedItems.Find(ETypeOfEquipmentSlot::Offhand);
	if (result != nullptr)
	{
		return true;
	}
	else
	{
		return false;
	}
}
/*
 checks if the given item type is a two handed weapon. returns true if it is, false otherwise.
*/
bool UEquippedItems::IsTwoHanded(ETypeOfItem mainHandItemType)
{
	if (mainHandItemType == ETypeOfItem::Axe_2H ||
		mainHandItemType == ETypeOfItem::Bow ||
		mainHandItemType == ETypeOfItem::Mace_2H ||
		mainHandItemType == ETypeOfItem::Scythe ||
		mainHandItemType == ETypeOfItem::Staff ||
		mainHandItemType == ETypeOfItem::Sword_2H)
	{
		return true;
	}
	else
	{
		return false;
	}
}

/*
checks if the given item type is a one handed weapon. returns true if it is, false otherwise.
*/
bool UEquippedItems::IsOneHanded(ETypeOfItem ItemType)
{
	if (ItemType == ETypeOfItem::Axe_1H ||
		ItemType == ETypeOfItem::Dagger ||
		ItemType == ETypeOfItem::Fist ||
		ItemType == ETypeOfItem::Mace_1H ||
		ItemType == ETypeOfItem::Shield_MH ||
		ItemType == ETypeOfItem::Sword_1H)
	{
		return true;
	}
	return false;
}

/*
	returns all equipped items
*/
TMap<ETypeOfEquipmentSlot, FGuid> UEquippedItems::GetEquippedItems()
{
	return MyEquippedItems;
}

/*
 Returns which slot the type of item should be equipped in. 
 has some logic for items such as rings, which have two slots,
 1h weapons, determining which slot they should be equipped in, as 1h weapons can be dual wielded.
 flasks, same as rings, but three slots.
*/
ETypeOfEquipmentSlot UEquippedItems::FindAssociatedEquipmentSlot(ETypeOfItem ItemType)
{
	switch (ItemType)
	{
	case ETypeOfItem::Back:					return ETypeOfEquipmentSlot::Back; break;
	case ETypeOfItem::Boots:				return ETypeOfEquipmentSlot::Boots;	break;
	case ETypeOfItem::Chest:				return ETypeOfEquipmentSlot::Chest;	break;
	case ETypeOfItem::Gloves:				return ETypeOfEquipmentSlot::Gloves; break;
	case ETypeOfItem::Helmet:				return ETypeOfEquipmentSlot::Helmet; break;
	case ETypeOfItem::Legs:					return ETypeOfEquipmentSlot::Legs;	break;
	case ETypeOfItem::Shoulder:				return ETypeOfEquipmentSlot::Shoulder;	break;
	case ETypeOfItem::Amulet:				return ETypeOfEquipmentSlot::Amulet;	break;
	case ETypeOfItem::Ancient_Relic:		return ETypeOfEquipmentSlot::Ancient_Relic;	break;
	case ETypeOfItem::Ring:
	{
		//check if there's an available ring slot. if not, default to slot 1.
		if (MyEquippedItems.Find(ETypeOfEquipmentSlot::Ring_1) == nullptr)
		{
			return ETypeOfEquipmentSlot::Ring_1;
		}
		else if (MyEquippedItems.Find(ETypeOfEquipmentSlot::Ring_2) == nullptr)
		{
			return ETypeOfEquipmentSlot::Ring_2;
		}
		else
		{
			return ETypeOfEquipmentSlot::Ring_1;
		}
	}
	case ETypeOfItem::Flask:
	{
		//check if there's an available flask slot. If not, default to slot 1.
		if (MyEquippedItems.Find(ETypeOfEquipmentSlot::Flask_1) == nullptr)
		{
			return ETypeOfEquipmentSlot::Flask_1;
		}
		else if (MyEquippedItems.Find(ETypeOfEquipmentSlot::Flask_2) == nullptr)
		{
			return ETypeOfEquipmentSlot::Flask_2;
		}
		else if (MyEquippedItems.Find(ETypeOfEquipmentSlot::Flask_3) == nullptr)
		{
			return ETypeOfEquipmentSlot::Flask_3;
		}
		else
		{
			return ETypeOfEquipmentSlot::Flask_1;
		}
		break;
	}
	case ETypeOfItem::Consumable:			return ETypeOfEquipmentSlot::NONE; break;
	case ETypeOfItem::Currency:				return ETypeOfEquipmentSlot::NONE; break;
	case ETypeOfItem::Axe_1H:
	{
		//check if dual wielding is availble.
		if (MyEquippedItems.Find(ETypeOfEquipmentSlot::Mainhand) == nullptr)
		{
			return ETypeOfEquipmentSlot::Mainhand;
		}
		else if (MyEquippedItems.Find(ETypeOfEquipmentSlot::Offhand) == nullptr)
		{
			return ETypeOfEquipmentSlot::Offhand;
		}
		else
		{
			return ETypeOfEquipmentSlot::Mainhand;
		}
		break;
	}
	case ETypeOfItem::Axe_2H:				return ETypeOfEquipmentSlot::Mainhand; 	break;
	case ETypeOfItem::Mace_1H:
	{
		//check if dual wielding is availble.
		if (MyEquippedItems.Find(ETypeOfEquipmentSlot::Mainhand) == nullptr)
		{
			return ETypeOfEquipmentSlot::Mainhand;
		}
		else if (MyEquippedItems.Find(ETypeOfEquipmentSlot::Offhand) == nullptr)
		{
			return ETypeOfEquipmentSlot::Offhand;
		}
		else
		{
			return ETypeOfEquipmentSlot::Mainhand;
		}
		break;
	}
	case ETypeOfItem::Mace_2H:				return ETypeOfEquipmentSlot::Mainhand; 	break;
	case ETypeOfItem::Sword_1H:
	{
		//check if dual wielding is availble.
		if (MyEquippedItems.Find(ETypeOfEquipmentSlot::Mainhand) == nullptr)
		{
			return ETypeOfEquipmentSlot::Mainhand;
		}
		else if (MyEquippedItems.Find(ETypeOfEquipmentSlot::Offhand) == nullptr)
		{
			return ETypeOfEquipmentSlot::Offhand;
		}
		else
		{
			return ETypeOfEquipmentSlot::Mainhand;
		}
		break;
	}
	case ETypeOfItem::Sword_2H:				return ETypeOfEquipmentSlot::Mainhand; 	break;
	case ETypeOfItem::Shield_MH:			return ETypeOfEquipmentSlot::Mainhand; 	break;
	case ETypeOfItem::Shield_OH:			return ETypeOfEquipmentSlot::Offhand; 	break;
	case ETypeOfItem::Bow:					return ETypeOfEquipmentSlot::Mainhand; 	break;
	case ETypeOfItem::Staff:				return ETypeOfEquipmentSlot::Mainhand; 	break;
	case ETypeOfItem::Scythe:				return ETypeOfEquipmentSlot::Mainhand; 	break;
	case ETypeOfItem::Dagger:
	{
		//check if dual wielding is availble.
		if (MyEquippedItems.Find(ETypeOfEquipmentSlot::Mainhand) == nullptr)
		{
			return ETypeOfEquipmentSlot::Mainhand;
		}
		else if (MyEquippedItems.Find(ETypeOfEquipmentSlot::Offhand) == nullptr)
		{
			return ETypeOfEquipmentSlot::Offhand;
		}
		else
		{
			return ETypeOfEquipmentSlot::Mainhand;
		}
		break;
	}
	case ETypeOfItem::Fist:
	{
		//check if dual wielding is availble.
		if (MyEquippedItems.Find(ETypeOfEquipmentSlot::Mainhand) == nullptr)
		{
			return ETypeOfEquipmentSlot::Mainhand;
		}
		else if (MyEquippedItems.Find(ETypeOfEquipmentSlot::Offhand) == nullptr)
		{
			return ETypeOfEquipmentSlot::Offhand;
		}
		else
		{
			return ETypeOfEquipmentSlot::Mainhand;
		}
		break;
	}
	case ETypeOfItem::Quiver:				return ETypeOfEquipmentSlot::Offhand; 	break;
	case ETypeOfItem::ElementalOrb:			return ETypeOfEquipmentSlot::Offhand; 	break;
	case ETypeOfItem::Spellbook:			return ETypeOfEquipmentSlot::Offhand; 	break;
	default: return ETypeOfEquipmentSlot::NONE; break;
	}
}
/*
	Function used for applying the equipped items data stored in the save game file to the statistics component for the player.
*/
bool UEquippedItems::EquipSavedItems(TMap<ETypeOfEquipmentSlot, FGuid> ItemsFromFile)
{
	AActor* MyPawn = GetOwner();
	AAvacynCharacter* MyCharacter = Cast<AAvacynCharacter>(MyPawn);
	if (MyCharacter)
	{
		for (auto itemId : ItemsFromFile)
		{
			FItem tempItem = MyCharacter->GetInventoryComponent()->GetInventoryItem(itemId.Value);

			Equip(itemId.Value, FindAssociatedEquipmentSlot(tempItem.Type));
		}
		return true;
	}
	return false;
}