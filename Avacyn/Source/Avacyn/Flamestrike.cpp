// Fill out your copyright notice in the Description page of Project Settings.

#include "Avacyn.h"
#include "BaseEnemy.h"
#include "AvacynCharacter.h"
#include "Flamestrike.h"

AFlamestrike::AFlamestrike()
{
	static ConstructorHelpers::FObjectFinder<UParticleSystem> ParticleSystemRef(TEXT("ParticleSystem'/Game/InfinityBladeEffects/Effects/FX_Skill_RockBurst/P_RBurst_Fire_Eruption_01.P_RBurst_Fire_Eruption_01'"));

	ParticleSystem = ParticleSystemRef.Object;

	ProjectileMovementComponent->MaxSpeed = 0.0f;
	ProjectileMovementComponent->InitialSpeed = 0.0f;
}

void AFlamestrike::BeginPlay()
{
	Super::BeginPlay();
	auto EmitterSpawned = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ParticleSystem, GetActorLocation(), GetActorRotation(), true);
	//sets the scale of the particles to match that of the actual aoe size.
	EmitterSpawned->SetRelativeScale3D(FVector(ProjectileBehaviorData.AoESize / 200.0f, ProjectileBehaviorData.AoESize / 200.0f, 1.0f));
}

void AFlamestrike::Tick(float DeltaSeconds)
{
	this->Destroy();
}

void AFlamestrike::OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	Super::OnOverlapBegin(OverlappedComp, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);
}

void AFlamestrike::HitActor(AActor* OtherActor)
{
	ABaseEnemy* EnemyHit = Cast<ABaseEnemy>(OtherActor);
	AAvacynCharacter* PlayerHit = Cast<AAvacynCharacter>(OtherActor);

	if (EnemyHit)
	{
		EnemyHit->ReceiveDamage(ProjectileDamageData);
	}
	else if (PlayerHit)
	{
		PlayerHit->ReceiveDamage(ProjectileDamageData);
	}
	else
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Something was hit, but casts failed!"));
	}
}

void AFlamestrike::Initialize()
{
	Super::Initialize();

	ProjectileCollisionSphere->SetSphereRadius(ProjectileBehaviorData.AoESize);
}