// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "AvacynCharacter.h"
#include "AvacynSaveGame.h"
#include "AvacynGameInstance.h"
#include "GameFramework/GameModeBase.h"
#include "AvacynGameMode.generated.h"


//TODO: fix naming convention.
UENUM(BlueprintType)
enum class EMenuTypes : uint8
{
	MT_TalentTree			= 0 UMETA(DisplayName = "TalentTree"),
	MT_SkillWindow			= 1 UMETA(DisplayName = "SkillWindow"),
	MT_Inventory			= 2 UMETA(DisplayName = "Inventory"),
	MT_EquipmentAndStats	= 3 UMETA(DisplayName = "EquipmentAndStats"),
	MT_QuestLog				= 4 UMETA(DisplayName = "QuestLog"),
	MT_DevelopmentTool	    = 5 UMETA(DisplayName = "Development Tool")
};

UCLASS(minimalapi)
class AAvacynGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AAvacynGameMode();

	void BeginPlay() override;

	/************************************************************************/
	/*                         Saving state		                            */
	/************************************************************************/
	void InitializeAutoSaveGame();

	UFUNCTION(BlueprintCallable, Category = "GameMode")
	void SaveCharacter();

	void LoadCharacter();
	
public:
	/************************************************************************/
	/*  Save state variables                                                */
	/************************************************************************/
	UAvacynGameInstance* MyGameInstance;
	USkeletalMesh* CharacterSkeletalMesh;
	AAvacynCharacter* MyPawn;

	/************************************************************************/
	/*  Opening and closing Menus:											*/
	/*  Inventory, EquipmentAndStats, QuestLog, TalentTree, SkillWindow     */
	/************************************************************************/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MenuSwitch")
	bool bOpenNewWindow;
	EMenuTypes WindowToOpen;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MenuSwitch")
	TArray<bool> CurrentlyOpen;
	
	UFUNCTION(BlueprintCallable, Category = "MenuSwitch")
	EMenuTypes GetWindowToOpen();

	UFUNCTION(BlueprintCallable, Category = "MenuSwitch")
	void SetCurrentlyOpen(EMenuTypes type, bool IsOpen);
	
	UFUNCTION(BlueprintCallable, Category = "MenuSwitch")
	bool IsCurrentlyOpen(EMenuTypes type);

	void openMenu(EMenuTypes type);

	/************************************************************************/
	/* Loot Generation                                                      */
	/************************************************************************/

public:
	UFUNCTION(BlueprintCallable, Category = "Loot")
	FItem GenerateItem(ETypeOfRarity ItemRarity, int ItemLevel);

	TMap<EStandardItemPrefix, FItemModifier> StandardItemPrefixes;

	TMap<ETypeOfStat, TArray<float>> MinStatRange;

private:
	void InitializeItemModifiers();

	void InitializeStatRanges();

	/************************************************************************/
	/* debug settings                                                       */
	/************************************************************************/

private:
	bool bDebugMode;
	/************************************************************************/
	/* Timer handler                                                        */
	/************************************************************************/

private:
	FTimerHandle TimerHandle;
};



