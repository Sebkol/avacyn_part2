// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"
#include "Inventory.h"
#include "Enums.h"
#include "Structs.h"
#include "EquippedItems.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class AVACYN_API UEquippedItems : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UEquippedItems();

	// Called when the game starts
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;

	UFUNCTION(BlueprintCallable, Category = "Equipped Items")
		bool EquipRequest(FGuid ItemId);
	UFUNCTION(BlueprintCallable, Category = "Equipped Items")
		bool UnequipRequest(ETypeOfEquipmentSlot slot);
	UFUNCTION(BlueprintCallable, Category = "Equipped Items")
		void Unequip(ETypeOfEquipmentSlot slot);
	UFUNCTION(BlueprintCallable, Category = "Equipped Items")
		bool Equip(FGuid newItemId, ETypeOfEquipmentSlot slot);
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Equipped Items")
		bool IsOffhandAvailable();
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Equipped Items")
		bool IsTwoHanded(ETypeOfItem mainHandItemType);
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Equipped Items")
		bool IsOneHanded(ETypeOfItem ItemType);
	UFUNCTION(BlueprintCallable, Category = "Equipped Items")
		TMap<ETypeOfEquipmentSlot, FGuid> GetEquippedItems();

	//equips the item from the save file when loading the character.
	bool EquipSavedItems(TMap<ETypeOfEquipmentSlot, FGuid> ItemsFromFile);
private:
	//only used inside the component. Therefore, the function will not be available to
	//blueprints.
	ETypeOfEquipmentSlot FindAssociatedEquipmentSlot(ETypeOfItem ItemType);
	
	TMap<ETypeOfEquipmentSlot, FGuid> MyEquippedItems;
};
