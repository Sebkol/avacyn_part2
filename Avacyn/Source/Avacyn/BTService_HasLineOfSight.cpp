// Fill out your copyright notice in the Description page of Project Settings.

#include "Avacyn.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyAllTypes.h"
#include "AvacynCharacter.h"
#include "EnemyAI.h"
#include "BaseEnemy.h"
#include "BTService_HasLineOfSight.h"



UBTService_HasLineOfSight::UBTService_HasLineOfSight()
{
	bCreateNodeInstance = true;
}

void UBTService_HasLineOfSight::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	AEnemyAI* EnemyPlayerController = Cast<AEnemyAI>(OwnerComp.GetAIOwner());
	if (EnemyPlayerController)
	{
		AAvacynCharacter* Enemy = Cast<AAvacynCharacter>(OwnerComp.GetBlackboardComponent()->GetValue<UBlackboardKeyType_Object>(EnemyPlayerController->EnemyKeyID));
		if (Enemy)
		{
			if (EnemyPlayerController->LineOfSightTo(Enemy))
			{
				GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Green, "Has line of sight to player!");
			}
			else
			{
				GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Red, "Does not have line of sight to player!");
			}
		}
	}
}