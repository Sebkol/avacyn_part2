// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIController.h"
#include "EnemyAI.generated.h"

/**
 * 
 */
UCLASS()
class AVACYN_API AEnemyAI : public AAIController
{
	GENERATED_BODY()
	
	UPROPERTY(transient)
	class UBlackboardComponent* BlackboardComponent;

	UPROPERTY(transient)
	class UBehaviorTreeComponent* BehaviorComponent;

public:
	AEnemyAI();

	virtual void Possess(APawn* InPawn) override;
	
	uint8 EnemyKeyID;
	uint8 DesiredRangeToTargetKeyID;

	float DesiredRangeToTarget;


	
};
