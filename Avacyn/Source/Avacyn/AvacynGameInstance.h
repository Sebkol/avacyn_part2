// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "TalentFromFile.h"
#include "Engine/GameInstance.h"
#include "AvacynGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class AVACYN_API UAvacynGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
	
public:
	UAvacynGameInstance(const FObjectInitializer& ObjectInitializer);

	bool IsNewCharacter();
	void InitializeSpellDetails(TArray<FDATAFROMFILE> AllTalents);

	//get functions
	FString GetCharacterFileName();
	int32	GetCharacterFileSlot();
	USkeletalMesh* GetCharacterSkeletalMesh();
	FString GetCharacterName();
	//set
	void SetCharacterFileName(FString NewSaveName);
	void SetCharacterFileSlot(int32 NewSaveSlot);
	void SetCharacterName(FString newName);
	void SetCharacterSkeletalMesh(USkeletalMesh* NewSkeletalMesh);
	/************************************************************************/
	/* Variables for creating new character or loading existing character	*/
	/* from saveGame.														*/
	/************************************************************************/
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "CharacterInformation")
	bool bIsNewCharacter;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "CharacterInformation")
	FString CharacterName;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "CharacterInformation")
	FString CharacterFileName;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "CharacterInformation")
	int32   CharacterFileSlot;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "CharacterInformation")
	USkeletalMesh* CharacterSkeletalMesh;

	/************************************************************************/
	/* debug settings                                                       */
	/************************************************************************/
	bool bIsDebugMode;

	bool IsDebugMode();

	/************************************************************************/
	/* Spell Details                                                       */
	/************************************************************************/
	// returns SpellDetails component
	FORCEINLINE class USpellDetails* GetSpellDetailsComponent() const { return SpellDetails; }
	
	// SpellDetails component
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Spell Details", meta = (AllowPrivateAccess = "true"))
	class USpellDetails* SpellDetails;

	//UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Talents from file", meta = (AllowPrivateAccess = "true"))
	//TArray<FDATAFROMFILE> AllTalents;
};
