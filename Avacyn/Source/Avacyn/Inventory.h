// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Statistics.h"
#include "Enums.h"
#include "Structs.h"
#include "Components/ActorComponent.h"
#include "Inventory.generated.h"



UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class AVACYN_API UInventory : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UInventory();

	// Called when the game starts
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;

	UFUNCTION(BlueprintCallable, Category = "Inventory")
	void AddToInventory(FItem NewItem);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Inventory")
	TArray<FString> GetIconNames();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Inventory")
	TArray<FItem> GetWholeInventory();

	//get a single item from the inventory. uses Guid of the item to find it. 
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Inventory")
	FItem GetInventoryItem(FGuid ItemID);

	UFUNCTION(BlueprintCallable, Category = "Inventory")
		void CreateNewItem(FGuid uuid,
						   ETypeOfItem Type,
						   ETypeOfRarity Rarity,
						   uint8 LevelRequirement,
						   FString Name,
						   FString FlavorText,
						   FString ImageReference,
						   FString MeshReference,
						   TArray<ETypeOfStat> TypesOfStat,
						   TArray<float> StatValues);
	//UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Inventory")
	void ApplySavedItems(TArray<FItem> SaveItems);

	UPROPERTY(BlueprintReadWrite, Category = "Inventory")
	TArray<FItem> ItemsInInventory;

	UPROPERTY(BlueprintReadOnly, Category = "Inventory")
	TMap<FString, UTexture2D*> InventoryIcons;

	UPROPERTY(BlueprintReadWrite, Category = "Inventory")
	bool bHaveNewItems;

	UFUNCTION(BlueprintCallable, Category = "Inventory")
	UTexture2D* FindIcon(FString Path);

	UFUNCTION(BlueprintCallable, Category = "Inventory")
	TArray<float> GetMapValues(FItem Target);
	
	UFUNCTION(BlueprintCallable, Category = "Inventory")
	TArray<ETypeOfStat> GetMapNames(FItem Target);

	UFUNCTION(BlueprintCallable, Category = "Inventory")
		bool DropItem(FItem ItemToDrop);
	UFUNCTION(BlueprintCallable, Category = "Inventory")
		bool AddItem(FItem ItemToAdd);

	void BroadcastNewItem();

	//DEVELOPMENT TOOL FUNCTIONS
	UFUNCTION(BlueprintCallable, Category = "Inventory")
	void CreateRandomItems(int32 NumberOfItemsToCreate);

private:
};
