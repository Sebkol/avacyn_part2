// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"
#include "Enums.h"
#include "Structs.h"
#include "TalentTree.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class AVACYN_API UTalentTree : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UTalentTree();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, Category = "TalentTree")
	FORCEINLINE TMap<ETypeOfTalentTree, FTalentTreeAllocationInfo> GetAllocatedTalents() { return AllocatedTalents; };

	//UFUNCTION(BlueprintCallable, Category = "TalentTree")
	void ApplyTalentInfoFromSave(TMap<ETypeOfTalentTree, FTalentTreeAllocationInfo> SaveTalentInfo);
	
	UFUNCTION(BlueprintCallable, Category = "TalentTree")
	void AllocatePointFromTalentTree(ETypeOfTalentTree TalentTree, ETalentName TalentName, uint8 AllocatedPoints);

	UFUNCTION(BlueprintCallable, Category = "TalentTree")
	uint8 GetNumPointsAllocated();
private:
	//a map containing a struct which holds information about every Talent within that specific talent tree.
	TMap<ETypeOfTalentTree, FTalentTreeAllocationInfo> AllocatedTalents;
};
