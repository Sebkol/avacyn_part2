// Fill out your copyright notice in the Description page of Project Settings.

#include "Avacyn.h"
#include "BaseEnemy.h"
#include "AvacynCharacter.h"
#include "Fireball.h"



AFireball::AFireball()
{
	UStaticMeshComponent* SphereVisual = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisualRepresentation"));
	SphereVisual->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UStaticMesh> SphereVisualAsset(TEXT("StaticMesh'/Game/InfinityBladeEffects/Effects/FX_Meshes/Fire/SM_FireBall.SM_FireBall'"));
	static ConstructorHelpers::FObjectFinder<UMaterial> Material(TEXT("Material'/Game/InfinityBladeEffects/Effects/FX_Materials/Fire/M_JaggedRockFire_MeshEmit_Lit.M_JaggedRockFire_MeshEmit_Lit'"));
	static ConstructorHelpers::FObjectFinder<UParticleSystem> ParticleSystem(TEXT("ParticleSystem'/Game/InfinityBladeEffects/Effects/FX_Combat_Base/Impact/P_Impact_Enemy_Fire_Strong.P_Impact_Enemy_Fire_Strong'"));
	if (SphereVisualAsset.Succeeded())
	{
		SphereVisual->SetStaticMesh(SphereVisualAsset.Object);
		SphereVisual->SetMaterial(0, Material.Object);
		SphereVisual->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f));
		SphereVisual->SetWorldScale3D(FVector(4.0f));
		SphereVisual->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}

	ImpactParticleSystem = ParticleSystem.Object;

	//ParticleComponent = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Impact Particles"));
	//ParticleComponent->AttachTo(RootComponent);
	//ParticleComponent->SetTemplate(ImpactParticleSystem.Object);
	
}

void AFireball::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AFireball::OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	Super::OnOverlapBegin(OverlappedComp, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);
}

void AFireball::HitActor(AActor* OtherActor)
{
	ABaseEnemy* EnemyHit = Cast<ABaseEnemy>(OtherActor);
	AAvacynCharacter* PlayerHit = Cast<AAvacynCharacter>(OtherActor);

	if (EnemyHit)
	{
		EnemyHit->ReceiveDamage(ProjectileDamageData);
	}
	else if (PlayerHit)
	{
		PlayerHit->ReceiveDamage(ProjectileDamageData);
	}
	else
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Something was hit, but casts failed!"));
	}

	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ImpactParticleSystem, GetActorLocation(), GetActorRotation(), true);

	this->Destroy();
}