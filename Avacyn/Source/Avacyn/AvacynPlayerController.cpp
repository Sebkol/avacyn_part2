// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "Avacyn.h"
#include "AvacynPlayerController.h"
#include "BaseProjectile.h"
#include "Fireball.h"
#include "DroppedItem.h"
#include "AI/Navigation/NavigationSystem.h"
#include "Runtime/Engine/Classes/Components/DecalComponent.h"
#include "Kismet/HeadMountedDisplayFunctionLibrary.h"
#include "AvacynCharacter.h"

AAvacynPlayerController::AAvacynPlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Default;
	//DefaultMouseCursor = EMouseCursor::Default;
	auto world = GetWorld();
	if (world)
	{
		auto GameMode = UGameplayStatics::GetGameMode(GetWorld());
		if (GameMode)
		{
			auto AvacynMode = Cast<AAvacynGameMode>(GameMode);
			if (AvacynMode)
			{
				myGameMode = AvacynMode;
			}
		}
	}

}

void AAvacynPlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);
	// keep updating the destination every tick while desired
	if (bMoveToMouseCursor)
	{
		MoveToMouseCursor();
	}

}

void AAvacynPlayerController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();

	InputComponent->BindAction("SetDestination", IE_Pressed, this, &AAvacynPlayerController::OnSetDestinationPressed);
	InputComponent->BindAction("SetDestination", IE_Released, this, &AAvacynPlayerController::OnSetDestinationReleased);
	if (myGameMode)
	{
		InputComponent->BindAction("Save", IE_Pressed, this, &AAvacynPlayerController::SaveGame);
		InputComponent->BindAction("Load", IE_Pressed, this, &AAvacynPlayerController::LoadGame);
		//InputComponent->BindAction("CharacterName", IE_Pressed, this, &AAvacynPlayerController::SetCharacterName);
		//InputComponent->BindAction("TalentTree", IE_Pressed, this, &[this]{this->MenuToOpen = EMenuTypes::MT_TalentTree; });
		InputComponent->BindAction("TalentTree", IE_Pressed, this, &AAvacynPlayerController::OpenMenuTalentTree);
		InputComponent->BindAction("Inventory",
			IE_Pressed,
			this,
			&AAvacynPlayerController::OpenMenuInventory);
		InputComponent->BindAction("EquipmentAndStats", IE_Pressed, this, &AAvacynPlayerController::OpenMenuEquipmentAndStats);
		InputComponent->BindAction("SkillWindow", IE_Pressed, this, &AAvacynPlayerController::OpenMenuSkillWindow);
		InputComponent->BindAction("QuestLog", IE_Pressed, this, &AAvacynPlayerController::OpenMenuQuestLog);
		InputComponent->BindAction("DevelopmentTool", IE_Pressed, this, &AAvacynPlayerController::OpenDevelopmentTool);
		InputComponent->BindAction("ActionBarButton1", IE_Pressed, this, &AAvacynPlayerController::TriggerInput1);
		InputComponent->BindAction("ActionBarButton2", IE_Pressed, this, &AAvacynPlayerController::TriggerInput2);
		InputComponent->BindAction("ActionBarButton3", IE_Pressed, this, &AAvacynPlayerController::TriggerInput3);
		InputComponent->BindAction("ActionBarButton4", IE_Pressed, this, &AAvacynPlayerController::TriggerInput4);
		InputComponent->BindAction("ActionBarButton5", IE_Pressed, this, &AAvacynPlayerController::TriggerInput5);
		InputComponent->BindAction("ActionBarButton6", IE_Pressed, this, &AAvacynPlayerController::TriggerInput6);
		InputComponent->BindAction("ActionBarButton7", IE_Pressed, this, &AAvacynPlayerController::TriggerInput7);
		InputComponent->BindAction("ActionBarButton8", IE_Pressed, this, &AAvacynPlayerController::TriggerInput8);
		InputComponent->BindAction("ActionBarButton9", IE_Pressed, this, &AAvacynPlayerController::TriggerInput9);
		InputComponent->BindAction("ActionBarButton0", IE_Pressed, this, &AAvacynPlayerController::TriggerInput0);

		if (GEngine)
		{
			GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, TEXT("Special keybindings successful"));
		}
	}
	// support touch devices 
	InputComponent->BindTouch(EInputEvent::IE_Pressed, this, &AAvacynPlayerController::MoveToTouchLocation);
	InputComponent->BindTouch(EInputEvent::IE_Repeat, this, &AAvacynPlayerController::MoveToTouchLocation);

	InputComponent->BindAction("ResetVR", IE_Pressed, this, &AAvacynPlayerController::OnResetVR);

	InputComponent->BindAction("SpellCastTest", IE_Pressed, this, &AAvacynPlayerController::OnSpellCastTestPressed);
}

void AAvacynPlayerController::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void AAvacynPlayerController::MoveToMouseCursor()
{
	if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
	{
		if (AAvacynCharacter* MyPawn = Cast<AAvacynCharacter>(GetPawn()))
		{
			if (MyPawn->GetCursorToWorld())
			{
				UNavigationSystem::SimpleMoveToLocation(this, MyPawn->GetCursorToWorld()->GetComponentLocation());
			}
		}
	}
	else
	{
		// Trace to see what is under the mouse cursor
		FHitResult Hit;
		GetHitResultUnderCursor(ECC_Visibility, false, Hit);

		if (Hit.bBlockingHit)
		{
			AActor* ActorHit = Hit.GetActor();
			ADroppedItem* Item = Cast<ADroppedItem>(ActorHit);
			if (Item)
			{
				//GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Purple, TEXT("somthing blocked "));

				// check if close enough
				AAvacynCharacter* MyCharacter = Cast<AAvacynCharacter>(GetPawn());
				if (MyCharacter)
				{
					auto DistanceToItem = MyCharacter->GetDistanceTo(Item);
					if (DistanceToItem < 200) //TODO: const that number somewhere.
					{
						//pick up item
						if (MyCharacter->GetInventoryComponent()->AddItem(Item->GetItemData()))
						{
							//GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Purple, TEXT("Successfully added item to inventory") + FString::SanitizeFloat(DistanceToItem));
							MyCharacter->GetInventoryComponent()->BroadcastNewItem();
							Item->Destroy(true);
						}

					}
					else
					{
						//GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Purple, TEXT("Distance to player: ") + FString::SanitizeFloat(DistanceToItem));
						SetNewMoveDestination(Hit.ImpactPoint);
					}
				}
			}
			// We hit something, move there
			else
			{
				SetNewMoveDestination(Hit.ImpactPoint);

			}
		}
	}
}

void AAvacynPlayerController::MoveToTouchLocation(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	FVector2D ScreenSpaceLocation(Location);

	// Trace to see what is under the touch location
	FHitResult HitResult;
	GetHitResultAtScreenPosition(ScreenSpaceLocation, CurrentClickTraceChannel, true, HitResult);
	if (HitResult.bBlockingHit)
	{
		// We hit something, move there
		SetNewMoveDestination(HitResult.ImpactPoint);
	}
}

void AAvacynPlayerController::SetNewMoveDestination(const FVector DestLocation)
{
	APawn* const MyPawn = GetPawn();
	if (MyPawn)
	{
		UNavigationSystem* const NavSys = GetWorld()->GetNavigationSystem();
		float const Distance = FVector::Dist(DestLocation, MyPawn->GetActorLocation());

		// We need to issue move command only if far enough in order for walk animation to play correctly
		if (NavSys && (Distance > 120.0f))
		{
			NavSys->SimpleMoveToLocation(this, DestLocation);
		}
	}
}

void AAvacynPlayerController::OnSetDestinationPressed()
{
	// set flag to keep updating destination until released
	bMoveToMouseCursor = true;
}

void AAvacynPlayerController::OnSetDestinationReleased()
{
	// clear flag to indicate we should stop updating the destination
	bMoveToMouseCursor = false;
}

void AAvacynPlayerController::OnSpellCastTestPressed()
{
	AAvacynCharacter* MyPawn = Cast<AAvacynCharacter>(GetControlledPawn());
	if (MyPawn)
	{
		FHitResult Hit;
		GetHitResultUnderCursor(ECC_Visibility, false, Hit);
		//test for spellcastingcomponent stuffz
		MyPawn->CastSpell(ESpellName::Fireball_rank1, Hit.ImpactPoint);

		// debug message: prints out the impact location of the mouse
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("Hit Impactpoint X: %f, Y: %f, Z: %f"), Hit.ImpactPoint.X, Hit.ImpactPoint.Y, Hit.ImpactPoint.Z));


		/*  OLD CODE - used before spellcastingComponent->CastSpell() was implemented
		FActorSpawnParameters SpawnInfo;
		SpawnInfo.Instigator = MyPawn;

		FVector Location = MyPawn->GetActorLocation();
		FVector HitLocation = Hit.ImpactPoint;
		HitLocation.Z += (Location.Z - HitLocation.Z);
		FVector Rotation(HitLocation - MyPawn->GetActorLocation());
		Rotation.Normalize();

		// debug message: prints out the impact location of the mouse
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("Hit Impactpoint X: %f, Y: %f, Z: %f"), Hit.ImpactPoint.X, Hit.ImpactPoint.Y, Hit.ImpactPoint.Z));

		FActorSpawnParameters SpawnInfo;
		SpawnInfo.Instigator = MyPawn;

		auto projectile = GetWorld()->SpawnActor<AFireball>(Location, Rotation.Rotation(), SpawnInfo);

		FProjectileBehaviorData NewProjectileBehaviorData;
		NewProjectileBehaviorData.MaxTravelDistance = 1000.0f;
		NewProjectileBehaviorData.ProjectileSpeed = 1200.0f;
		FProjectileDamageData NewProjectileDamageData;
		NewProjectileDamageData.InstigatorLevel = MyPawn->GetStatisticsComponent()->GetLevel();
		NewProjectileDamageData.Damage.Add(ETypeOfDamage::Fire, 50.0f);
		NewProjectileDamageData.Damage.Add(ETypeOfDamage::Frost, 10.0f);
		NewProjectileDamageData.DamagePenetrationPercentage.Add(ETypeOfDamage::Fire, 0.0f);
		NewProjectileDamageData.EliteDamageModifier = 2.0f;
		NewProjectileDamageData.Instigator = GetControlledPawn();

		// debug message: check if projectile was successfully spawned or not
		if (projectile)
		{
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, TEXT("Spawn successfull!"));
		}

		//*/
	}
}

void AAvacynPlayerController::SaveGame() {
	if (myGameMode)
	{
		myGameMode->SaveCharacter();
	}
}
void AAvacynPlayerController::LoadGame() {
	if (myGameMode)
	{
		myGameMode->LoadCharacter();
	}
}

void AAvacynPlayerController::OpenMenuTalentTree()
{
	myGameMode->openMenu(EMenuTypes::MT_TalentTree);
}
void AAvacynPlayerController::OpenMenuInventory()
{
	myGameMode->openMenu(EMenuTypes::MT_Inventory);
}
void AAvacynPlayerController::OpenMenuSkillWindow()
{
	myGameMode->openMenu(EMenuTypes::MT_SkillWindow);
}
void AAvacynPlayerController::OpenMenuEquipmentAndStats()
{
	myGameMode->openMenu(EMenuTypes::MT_EquipmentAndStats);
}
void AAvacynPlayerController::OpenMenuQuestLog()
{
	myGameMode->openMenu(EMenuTypes::MT_QuestLog);
}
void AAvacynPlayerController::OpenDevelopmentTool()
{
	myGameMode->openMenu(EMenuTypes::MT_DevelopmentTool);
}

void AAvacynPlayerController::TriggerInput1()
{
	FHitResult Hit;
	GetHitResultUnderCursor(ECC_Visibility, false, Hit);
	TargetLocation = Hit.ImpactPoint;
	keyPressed = 1;
	CastSpell = true;
}
void AAvacynPlayerController::TriggerInput2()
{
	FHitResult Hit;
	GetHitResultUnderCursor(ECC_Visibility, false, Hit);
	TargetLocation = Hit.ImpactPoint;
	keyPressed = 2;
	CastSpell = true;
}
void AAvacynPlayerController::TriggerInput3()
{
	FHitResult Hit;
	GetHitResultUnderCursor(ECC_Visibility, false, Hit);
	TargetLocation = Hit.ImpactPoint;
	keyPressed = 3;
	CastSpell = true;
}
void AAvacynPlayerController::TriggerInput4()
{
	FHitResult Hit;
	GetHitResultUnderCursor(ECC_Visibility, false, Hit);
	TargetLocation = Hit.ImpactPoint;
	keyPressed = 4;
	CastSpell = true;
}
void AAvacynPlayerController::TriggerInput5()
{
	FHitResult Hit;
	GetHitResultUnderCursor(ECC_Visibility, false, Hit);
	TargetLocation = Hit.ImpactPoint;
	keyPressed = 5;
	CastSpell = true;
}
void AAvacynPlayerController::TriggerInput6()
{
	FHitResult Hit;
	GetHitResultUnderCursor(ECC_Visibility, false, Hit);
	TargetLocation = Hit.ImpactPoint;
	keyPressed = 6;
	CastSpell = true;
}
void AAvacynPlayerController::TriggerInput7()
{
	FHitResult Hit;
	GetHitResultUnderCursor(ECC_Visibility, false, Hit);
	TargetLocation = Hit.ImpactPoint;
	keyPressed = 7;
	CastSpell = true;
}
void AAvacynPlayerController::TriggerInput8()
{
	FHitResult Hit;
	GetHitResultUnderCursor(ECC_Visibility, false, Hit);
	TargetLocation = Hit.ImpactPoint;
	keyPressed = 8;
	CastSpell = true;
}
void AAvacynPlayerController::TriggerInput9()
{
	FHitResult Hit;
	GetHitResultUnderCursor(ECC_Visibility, false, Hit);
	TargetLocation = Hit.ImpactPoint;
	keyPressed = 9;
	CastSpell = true;
}
void AAvacynPlayerController::TriggerInput0()
{
	FHitResult Hit;
	GetHitResultUnderCursor(ECC_Visibility, false, Hit);
	TargetLocation = Hit.ImpactPoint;
	keyPressed = 0;
	CastSpell = true;
}