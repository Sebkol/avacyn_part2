// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Healable.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UHealable : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};

/**
 * 
 */
class AVACYN_API IHealable
{
	GENERATED_IINTERFACE_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	
	//classes using this interface must implement ReceiveHealing
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Interface")
	bool ReceiveHealing();
	
};
