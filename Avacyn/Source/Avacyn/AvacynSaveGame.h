// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Inventory.h"
#include "GameFramework/SaveGame.h"
#include "AvacynSaveGame.generated.h"

/**
 * 
 */
UCLASS()
class AVACYN_API UAvacynSaveGame : public USaveGame
{
	GENERATED_BODY()
	
public:
	
	UAvacynSaveGame();



	/************************************************************************/
	/* Get functions for the saved variables                                */
	/************************************************************************/
	FVector GetPlayerLocation();
	
	UFUNCTION(BlueprintCallable, Category = "CharacterInformation")
	FString getCharacterName();

	UFUNCTION(BlueprintCallable, Category = "CharacterInformation")
	USkeletalMesh* GetCharacterSkeletalMesh();
	/************************************************************************/
	/* Set functions for the saved variables                                */
	/************************************************************************/
	void SetPlayerLocation(FVector newLocation);
	void SetCharacterName(FString newName);

public:


	/************************************************************************/
	/* Save variables                                                       */
	/************************************************************************/
	UPROPERTY(VisibleAnywhere, Category = "SaveData")
	FString SaveFileName;
	
	UPROPERTY(VisibleAnywhere, Category = "SaveData")
	int32 SaveFileSlot;

	UPROPERTY(VisibleAnywhere, Category = "SaveData")
	TArray<FItem> SaveInventoryItems;
	/************************************************************************/
	/* Character data to save                                               */
	/************************************************************************/
	UPROPERTY(VisibleAnywhere, Category = "SaveData")
	FVector PlayerLocation;

	UPROPERTY(VisibleAnywhere, Category = "SaveData")
	FString CharacterName;

	// Mesh data
	UPROPERTY(VisibleAnywhere, Category = "SaveData")
	USkeletalMesh* CharacterSkeletalMesh;

	//Talent information
	UPROPERTY(VisibleAnywhere, Category = "SaveData")
	TMap<ETypeOfTalentTree, FTalentTreeAllocationInfo> SaveTalentInfo;

	//Equipped Items 
	UPROPERTY(VisibleAnywhere, Category = "SaveData")
	TMap<ETypeOfEquipmentSlot, FGuid> SaveEquippedItems;
};
