// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BaseProjectile.h"
#include "Flamestrike.generated.h"

/**
 * 
 */
UCLASS()
class AVACYN_API AFlamestrike : public ABaseProjectile
{
	GENERATED_BODY()

public:

	AFlamestrike();

	virtual void BeginPlay() override;

	virtual void Tick(float DeltaSeconds) override;

	UFUNCTION()
	virtual void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;
	
	virtual void HitActor(AActor* OtherActor) override;

	virtual void Initialize() override;

	UParticleSystem* ParticleSystem;
};
