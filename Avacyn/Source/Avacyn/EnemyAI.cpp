// Fill out your copyright notice in the Description page of Project Settings.

#include "Avacyn.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyAllTypes.h"
#include "BaseEnemy.h"
#include "EnemyAI.h"




AEnemyAI::AEnemyAI()
{
	BlackboardComponent = CreateDefaultSubobject<UBlackboardComponent>(TEXT("Blackboard Component"));

	BehaviorComponent = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("Behavior Tree Component"));

	DesiredRangeToTarget = 600.0f;
}

void AEnemyAI::Possess(APawn* InPawn)
{
	Super::Possess(InPawn);

	ABaseEnemy *Character = Cast<ABaseEnemy>(InPawn);

	if (Character && Character->EnemyBehavior)
	{
		BlackboardComponent->InitializeBlackboard(*Character->EnemyBehavior->BlackboardAsset);

		EnemyKeyID = BlackboardComponent->GetKeyID("Target");
		DesiredRangeToTargetKeyID = BlackboardComponent->GetKeyID("DesiredRangeToTarget");

		BehaviorComponent->StartTree(*Character->EnemyBehavior);
	}

}