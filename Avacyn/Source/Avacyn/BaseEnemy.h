// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "Damageable.h"
#include "Healable.h"
#include "Statistics.h"
#include "SpellCastingComponent.h"
#include "GameplayTagContainer.h"
#include "BaseEnemy.generated.h"

UCLASS()
class AVACYN_API ABaseEnemy : public ACharacter, public IDamageable, public IHealable
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ABaseEnemy();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Returns Spellcasting subobject
	FORCEINLINE class USpellCastingComponent* GetSpellCastingComponent() const { return SpellCastingComponent; }

	FORCEINLINE class UStatistics* GetStatisticsComponent() const { return Statistics; }
	//FORCEINLINE class UWidgetComponent* GetTestComponent() const { return Test; }

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(EditAnywhere, Category = Behavior)
	class UBehaviorTree *EnemyBehavior;

	//ReceiveDamage interface
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Interface")
		bool ReceiveDamage(FProjectileDamageData ProjectileDamageData);
	virtual bool ReceiveDamage_Implementation(FProjectileDamageData ProjectileDamageData) override;

	//ReceiveHealing interface
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Interface")
		bool ReceiveHealing();
	virtual bool ReceiveHealing_Implementation() override;

	void Die();

	// Controllers call this to make a character cast spells
	void CastSpell(ESpellName Spell, FVector TargetLocation);

private:
	// Statistics component
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Statistics", meta = (AllowPrivateAccess = "true"))
	class UStatistics* Statistics;

	// SpellCasting component
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Spells", meta = (AllowPrivateAccess = "true"))
	class USpellCastingComponent* SpellCastingComponent;


	//IMPLEMENT WHEN RELATIVE POSITION BUG IS FIXED BY EPIC
	//UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Statistics", meta = (AllowPrivateAccess = "true"))
	//class UWidgetComponent* Test;
};
