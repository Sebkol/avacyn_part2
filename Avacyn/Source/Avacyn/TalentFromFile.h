// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "Engine/DataAsset.h"
#include "TalentFromFile.generated.h"

/**
 * 
 */


USTRUCT(BlueprintType)
struct FDATAFROMFILE : public FTableRowBase
{
	GENERATED_BODY()
	//TODO: have a prefix for determining which 'struct' the variable belongs to. useful to make code clearer.

	// General info
	UPROPERTY(BlueprintReadOnly, Category = "FDATAFROMFILE")
	FString Name;
	UPROPERTY(BlueprintReadOnly, Category = "FDATAFROMFILE")
	FString Description;
	UPROPERTY(BlueprintReadOnly, Category = "FDATAFROMFILE")
	FString TypeOfTalent;
	UPROPERTY(BlueprintReadOnly, Category = "FDATAFROMFILE")
	FString Icon;
	UPROPERTY(BlueprintReadOnly, Category = "FDATAFROMFILE")
	FString CanBeAugmentedBy;
	UPROPERTY(BlueprintReadOnly, Category = "FDATAFROMFILE")
	int Tier;
	UPROPERTY(BlueprintReadOnly, Category = "FDATAFROMFILE")
	int Rank;
	//Should the talent apply to everything or something specific?
	UPROPERTY(BlueprintReadOnly, Category = "FDATAFROMFILE")
	bool bIsGlobal;
	//Is the entry an actual talent? (augmented spells are not talents)
	UPROPERTY(BlueprintReadOnly, Category = "FDATAFROMFILE")
	bool bIsTalent;
	UPROPERTY(BlueprintReadOnly, Category = "FDATAFROMFILE")
	FString TalentTree;
	//Base spell info
	UPROPERTY(BlueprintReadOnly, Category = "FDATAFROMFILE")
	FString TypeOfDamage;
	UPROPERTY(BlueprintReadOnly, Category = "FDATAFROMFILE")
	float BaseDamageMin;
	UPROPERTY(BlueprintReadOnly, Category = "FDATAFROMFILE")
	float BaseDamageMax;
	UPROPERTY(BlueprintReadOnly, Category = "FDATAFROMFILE")
	int NumberOfProjectiles;
	UPROPERTY(BlueprintReadOnly, Category = "FDATAFROMFILE")
	int ProjectileSpeed;
	UPROPERTY(BlueprintReadOnly, Category = "FDATAFROMFILE")
	float Range;
	UPROPERTY(BlueprintReadOnly, Category = "FDATAFROMFILE")
	int NumberOfBouncesAndChains;
	UPROPERTY(BlueprintReadOnly, Category = "FDATAFROMFILE")
	float Cooldown;
	UPROPERTY(BlueprintReadOnly, Category = "FDATAFROMFILE")
	float CastTime;
	UPROPERTY(BlueprintReadOnly, Category = "FDATAFROMFILE")
	float BaseAoeSize;
	UPROPERTY(BlueprintReadOnly, Category = "FDATAFROMFILE")
	float StatEfficiency;
	UPROPERTY(BlueprintReadOnly, Category = "FDATAFROMFILE")
	FString ResourceType;
	UPROPERTY(BlueprintReadOnly, Category = "FDATAFROMFILE")
	int ResourceCost;
	UPROPERTY(BlueprintReadOnly, Category = "FDATAFROMFILE")
	FString ClassToSpawn;
	UPROPERTY(BlueprintReadOnly, Category = "FDATAFROMFILE")
	FString AnimationToUse;
	//DOT EFFECTS
	UPROPERTY(BlueprintReadOnly, Category = "FDATAFROMFILE")
	FString TypeOfDOT;
	UPROPERTY(BlueprintReadOnly, Category = "FDATAFROMFILE")
	float ChanceToApply;
	UPROPERTY(BlueprintReadOnly, Category = "FDATAFROMFILE")
	float DOTDuration;
	UPROPERTY(BlueprintReadOnly, Category = "FDATAFROMFILE")
	float DOTTickInterval;
	UPROPERTY(BlueprintReadOnly, Category = "FDATAFROMFILE")
	float DOTDamageMin;
	UPROPERTY(BlueprintReadOnly, Category = "FDATAFROMFILE")
	float DOTDamageMax;

	//Status effects
	UPROPERTY(BlueprintReadOnly, Category = "FDATAFROMFILE")
	FString TypeOfEffect;
	UPROPERTY(BlueprintReadOnly, Category = "FDATAFROMFILE")
	float EffectDuration;
	UPROPERTY(BlueprintReadOnly, Category = "FDATAFROMFILE")
	FString BuffEffect;
	UPROPERTY(BlueprintReadOnly, Category = "FDATAFROMFILE")
	FString CurseEffect;
	UPROPERTY(BlueprintReadOnly, Category = "FDATAFROMFILE")
	FString DebuffEffect;

	//CC effects
	UPROPERTY(BlueprintReadOnly, Category = "FDATAFROMFILE")
	FString TypeOfCC;
	UPROPERTY(BlueprintReadOnly, Category = "FDATAFROMFILE")
	float ChanceToApplyCC;
	UPROPERTY(BlueprintReadOnly, Category = "FDATAFROMFILE")
	float CCDuration;
	//passive
	UPROPERTY(BlueprintReadOnly, Category = "FDATAFROMFILE")
	FString TypeOfPassive;
	UPROPERTY(BlueprintReadOnly, Category = "FDATAFROMFILE")
	FString BonusAppliesTo;
	UPROPERTY(BlueprintReadOnly, Category = "FDATAFROMFILE")
	float BonusValue;

};

UCLASS()
class AVACYN_API UTalentFromFile : public UDataAsset
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere)
	TArray<FDATAFROMFILE> Data;

};
